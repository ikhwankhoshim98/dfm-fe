<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// dashboard
Route::group(['prefix' => 'dashboards'], function () {
    Route::get('/environment-monitor', "dashboardController@environmentMonitor")->name('dashboards.environmentMonitor');
    Route::get('/environment-monitor-b2', "dashboardController@environmentMonitorB2")->name('dashboards.environmentMonitorB2');
    Route::get('/inventory', "dashboardController@inventory")->name('dashboards.inventory');
    Route::get('/inventory-b2', "dashboardController@inventoryB2")->name('dashboards.inventoryB2');
    Route::get('/infra', "dashboardController@infra")->name('dashboards.infra');
    Route::get('/infra-b2', "dashboardController@infraB2")->name('dashboards.infraB2');
});
Route::resource('dashboards', "dashboardController");

//manual auto
Route::group(['prefix' => 'controls'], function () {
    Route::get('/broiler-temperature', "manualAutoController@broilerTemperature")->name('controls.broilerTemperature');
    Route::get('/ventilation-system', "manualAutoController@ventilationSystem")->name('controls.ventilationSystem');
    Route::get('/light-dimmer', "manualAutoController@lightDimmer")->name('controls.lightDimmer');
    Route::get('/auto-feeder-system', "manualAutoController@autoFeederSystem")->name('controls.autoFeederSystem');
    Route::get('/water-dosing-system', "manualAutoController@waterDosing")->name('controls.waterDosing');
    Route::get('/heater', "manualAutoController@heater")->name('controls.heater');
    Route::get('/static-pressure', "manualAutoController@staticPressure")->name('controls.staticPressure');
    Route::get('/tunnel-curtain', "manualAutoController@tunnelCurtain")->name('controls.tunnelCurtain');

});
Route::resource('controls', "manualAutoController");

//inventory
Route::group(['prefix' => 'inventories'], function () {
    Route::get('/doc', "inventoryController@doc")->name('inventories.doc');
    Route::get('/feed', "inventoryController@feed")->name('inventories.feed');
    Route::get('/supplements', "inventoryController@supplements")->name('inventories.supplements');
    Route::get('/cleaning-chemicals', "inventoryController@cleaningChemicals")->name('inventories.cleaningChemicals');

});
Route::resource('inventories', "inventoryController");

//addons
Route::group(['prefix' => 'addOns'], function () {
    Route::get('/chart', "addOnController@chart")->name('addOns.chart');
    Route::get('/cycle-record', "addOnController@cycleRecord")->name('addOns.cycleRecord');

});
Route::resource('addOns', "addOnController");

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
