<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Movie;

class Actor extends Model
{
    use HasFactory;

    // belong to relationship (foreign key)

    public function movie(){
        return $this->belongsTo(Movie::class, 'movie_id');
    }

    // has relationship

    public function movieM(){
        return $this->hasMany(Movie::class);
    }

}
