<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Actor;

use DateTimeInterface;

class Movie extends Model
{
    use HasFactory;

    protected $table = 'movies';
    protected $fillable = [
        'm_name','m_plot', 'm_year_release','m_poster', 'created_at', 'updated_at'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    // belong to relationship (foreign key)


    public function actor()
    {
        return $this->belongsTo(Actor::class, 'actor_id');
    }

    // has relationship

    public function actorM(){
        return $this->hasMany(actor::class);
    }


}
