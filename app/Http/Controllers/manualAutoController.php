<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class manualAutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    //broiler temperature
    public function broilerTemperature()
    {
        return view('controls.broiler-temperature');

    }
    //ventilation system
    public function ventilationSystem()
    {
        return view('controls.ventilation-system');
    }
    //light dimmer
    public function lightDimmer()
    {
        return view('controls.light-dimmer');
    }
    //automatic feeder
    public function autoFeederSystem()
    {
        return view('controls.auto-feeder-system');
    }
    //water dosing
    public function waterDosing()
    {
        return view('controls.water-dosing');
    }
    //heaters
    public function heater()
    {
        return view('controls.heater');
    }
    //static pressure
    public function staticPressure()
    {
        return view('controls.static-pressure');
    }

    //tunnel curtains
    public function tunnelCurtain()
    {
        return view('controls.tunnel-curtain');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
