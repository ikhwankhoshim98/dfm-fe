<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class dashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //main dashbboard view
    public function index()
    {
        return view('dashboards.index');
    }

    //dashboard views
    public function environmentMonitor()
    {
        // dd("hello world");
        return view('dashboards.environment-monitor');
    }

    public function environmentMonitorB2()
    {
        // dd("hello world");
        return view('dashboards.environment-monitor-b2');
    }

    public function inventory()
    {
        return view('dashboards.inventory');
    }

    public function inventoryB2()
    {
        return view('dashboards.inventory-b2');
    }

    public function infra()
    {
        return view('dashboards.infra');
    }

    public function infraB2()
    {
        return view('dashboards.infra-b2');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
