//chart index B1
var xValues = [50,60,70,80,90,100,110,120,130,140,150];
var yValues = [29, 28.9, 29.3, 29.6, 29.1, 29, 29, 28.9, 29.1, 28.2, 27.9];

new Chart("indexChartB1", {
  type: "line",
  data: {
    labels: xValues,
    datasets: [{
      fill: false,
      lineTension: 0.1,
      backgroundColor: "RGBA(28,200,138,1)",
    //   borderColor: "RGBA(28,200,138,1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      yAxes: [{ticks: {min: 27, max:31}}],
    }
  }
});
//chart index B2
var xValues = [50,60,70,80,90,100,110,120,130,140,150];
var yValues = [29, 28.9, 29.3, 29.6, 29.1, 29, 29, 28.9, 29.1, 28.2, 27.9];

new Chart("indexChartB2", {
  type: "line",
  data: {
    labels: xValues,
    datasets: [{
      fill: false,
      lineTension: 0.1,
      backgroundColor: "RGBA(28,200,138,1)",
    //   borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      yAxes: [{ticks: {min: 27, max:31}}],
    }
  }
});
//chart bird B1
var xValues = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];
var yValues = [1.5, 1.7, 1.4, 1.8, 1.5, 1.9, 1.5, 1.4, 1.3, 1.7];

new Chart("birdChartB1", {
  type: "line",
  data: {
    labels: xValues,
    datasets: [{
      fill: false,
      lineTension: 0.1,
      backgroundColor: "RGBA(28,200,138,1)",
    //   borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      yAxes: [{ticks: {min: 1, max:2}}],
    }
  }
});
//chart bird B2
var xValues = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];
var yValues = [1.5, 1.7, 1.4, 1.8, 1.5, 1.9, 1.5, 1.4, 1.3, 1.7];

new Chart("birdChartB2", {
  type: "line",
  data: {
    labels: xValues,
    datasets: [{
      fill: false,
      lineTension: 0.1,
      backgroundColor: "RGBA(28,200,138,1)",
    //   borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      yAxes: [{ticks: {min: 1, max:2}}],
    }
  }
});