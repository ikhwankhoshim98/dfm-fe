// switch on fan
function onFan() { 
var checkboxes = $('input:checkbox:checked').length;
var allSeatsVals = [];
if ($("input:checkbox:checked").length > 0)
    {
    //Storing in Array
    $('#seatsBlock :checked').each(function() {
    allSeatsVals.push($(this).val());
    });

    Swal.fire({
    title: 'Total fans: ' + checkboxes,
    text: 'Fans ID: ' + allSeatsVals,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Continue'
    }).then((result) => {
    if (result.isConfirmed) {
        Swal.fire(
        'Success',
        'Fans ' +allSeatsVals+' switched ON successfully',
        'success'
        )
    }
    })

    console.log(checkboxes + " fans chosen");
    console.log("fans ID: "+ allSeatsVals);
    
    //Displaying 
    $('#NumberDisplay').val(checkboxes);
    $('#seatsDisplay').val(allSeatsVals);
    }
else
{
    Swal.fire({
    icon: 'error',
    title: 'No Fan Selected!',
    text: 'Please select the fan first.',
    })
}
}
// switch off fan
function offFan() { 
    var checkboxes = $('input:checkbox:checked').length;
    var allSeatsVals = [];
    if ($("input:checkbox:checked").length > 0)
        {
        //Storing in Array
        $('#seatsBlock :checked').each(function() {
        allSeatsVals.push($(this).val());
        });
    
        Swal.fire({
        title: 'Total fans: ' + checkboxes,
        text: 'Fans ID: ' + allSeatsVals,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
        }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
            'Success',
            'Fans ' +allSeatsVals+' switched OFF successfully',
            'success'
            )
        }
        })
    
        console.log(checkboxes + " fans chosen");
        console.log("fans ID: "+ allSeatsVals);
        
        //Displaying 
        $('#NumberDisplay').val(checkboxes);
        $('#seatsDisplay').val(allSeatsVals);
        }
    else
    {
        Swal.fire({
        icon: 'error',
        title: 'No Fan Selected!',
        text: 'Please select the fan first.',
        })
    }
    }