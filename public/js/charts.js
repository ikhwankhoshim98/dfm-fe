//daily curve/graph
var speedCanvas = document.getElementById("dailyTemperature");

var dataFirst = {
    label: "Temperature",
    data: [29.8, 29, 29.4, 29, 29.6, 30, 29.8, 28.9, 29, 28.8, 29.9],
    lineTension: 0.3,
    fill: false,
    pointBackgroundColor: 'red',
    pointBorderColor:'red',
    backgroundColor: "rgba(25, 111, 61)",
    borderColor: 'red'
  };

var dataSecond = {
    label: "Humidity",
    data: [30, 30.5, 31, 30.2, 30.1, 29.9, 32.3, 30.9, 29, 29.2],
    lineTension: 0.3,
    fill: false,
    pointBackgroundColor: 'blue',
    pointBorderColor:'blue',
    backgroundColor: "rgba(25, 111, 61)",
    borderColor: 'blue'
  };

var speedData = {
  labels: ["0000", "0030", "0100", "0130", "0200", "0230", "0300", "0330", "0400", "0430"],
  datasets: [dataFirst, dataSecond]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 10,
    }
  }
};

var lineChart = new Chart(speedCanvas, {
  type: 'line',
  data: speedData,
  options: chartOptions
});

//cycle curve/graph
var speedCanvas = document.getElementById("cycleTemperature");

var dataFirst = {
    label: "Temperature",
    data: [29.8, 29, 29.4, 29, 29.6, 30, 29.8, 28.9, 29, 28.8, 29.9],
    lineTension: 0.3,
    fill: false,
    pointBackgroundColor: 'red',
    pointBorderColor:'red',
    backgroundColor: "rgba(25, 111, 61)",
    borderColor: 'red'
  };

var dataSecond = {
    label: "Humidity",
    data: [30, 30.5, 31, 30.2, 30.1, 29.9, 32.3, 30.9, 29, 29.2],
    lineTension: 0.3,
    fill: false,
    pointBackgroundColor: 'blue',
    pointBorderColor:'blue',
    backgroundColor: "rgba(25, 111, 61)",
    borderColor: 'blue'
  };

var speedData = {
  labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,],
  datasets: [dataFirst, dataSecond]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 10,
    }
  }
};

var lineChart = new Chart(speedCanvas, {
  type: 'line',
  data: speedData,
  options: chartOptions
});

