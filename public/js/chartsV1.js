//temperature charts
//daily chart
var xValues = ["0000", "0030", "0100", "0130", "0200", "0230", "0300", "0330", "0400", "0430", "0500", "0530", "0600", "0630", "0700", "0730", "0800", "0830", "0900", "0930", "1000", "1030", "1100", "1130", "1200"];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31, 31, 28, 28.8, 28.5, 28.4, 27, 26.9, 27, 28, 28.9, 29.1, 29, 29, 29, 29, 29];

new Chart("dailyTemperature", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//cycle chart
var xValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31];

new Chart("cycleTemperature", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//humidity charts
//daily chart
var xValues = ["0000", "0030", "0100", "0130", "0200", "0230", "0300", "0330", "0400", "0430", "0500", "0530", "0600", "0630", "0700", "0730", "0800", "0830", "0900", "0930", "1000", "1030", "1100", "1130", "1200"];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31, 31, 28, 28.8, 28.5, 28.4, 27, 26.9, 27, 28, 28.9, 29.1, 29, 29, 29, 29, 29];

new Chart("dailyHumidity", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//cycle chart
var xValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31];

new Chart("cycleHumidity", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//pressure & static charts
//daily chart
var xValues = ["0000", "0030", "0100", "0130", "0200", "0230", "0300", "0330", "0400", "0430", "0500", "0530", "0600", "0630", "0700", "0730", "0800", "0830", "0900", "0930", "1000", "1030", "1100", "1130", "1200"];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31, 31, 28, 28.8, 28.5, 28.4, 27, 26.9, 27, 28, 28.9, 29.1, 29, 29, 29, 29, 29];

new Chart("dailyPressure", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//cycle chart
var xValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31];

new Chart("cyclePressure", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//CO2 charts
//daily chart
var xValues = ["0000", "0030", "0100", "0130", "0200", "0230", "0300", "0330", "0400", "0430", "0500", "0530", "0600", "0630", "0700", "0730", "0800", "0830", "0900", "0930", "1000", "1030", "1100", "1130", "1200"];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31, 31, 28, 28.8, 28.5, 28.4, 27, 26.9, 27, 28, 28.9, 29.1, 29, 29, 29, 29, 29];

new Chart("dailyCO2", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//cycle chart
var xValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31];

new Chart("cycleCO2", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//ammonia charts
//daily chart
var xValues = ["0000", "0030", "0100", "0130", "0200", "0230", "0300", "0330", "0400", "0430", "0500", "0530", "0600", "0630", "0700", "0730", "0800", "0830", "0900", "0930", "1000", "1030", "1100", "1130", "1200"];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31, 31, 28, 28.8, 28.5, 28.4, 27, 26.9, 27, 28, 28.9, 29.1, 29, 29, 29, 29, 29];

new Chart("dailyAmmonia", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});

//cycle chart
var xValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];
var yValues = [29, 29.6, 30, 27, 28, 29, 28.8, 29.9, 31];

new Chart("cycleAmmonia", {
  type: "line",
  data: {
    labels: xValues,
    lineTension: 0.1,
    datasets: [{
      label: "Temperature",
      fill: false,
      backgroundColor: "rgba(25, 111, 61)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      xAxes: [{ticks: {min: 1, max:40}}],
      yAxes: [{ticks: {min: 1, max:40}}],
    }
  }
});