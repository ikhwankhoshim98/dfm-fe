<!-- sidebar -->
<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{ route('dashboards.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    System Overview
                </a>
                <!-- dashboard lists -->
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseDashboards" aria-expanded="false" aria-controls="collapseDashboards">
                    <div class="sb-nav-link-icon"><i class="far fa-list-alt"></i></div>
                    Dashboard Lists
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseDashboards" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseSubDashboards" aria-expanded="false" aria-controls="collapseDashboards">
                            <div class="sb-nav-link-icon"></div>
                            Environment Monitoring
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseSubDashboards" >
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('dashboards.environmentMonitor') }}">Boiler 1</a>
                                <a class="nav-link" href="{{ route('dashboards.environmentMonitorB2') }}">Boiler 2</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseSubDashboards2" aria-expanded="false" aria-controls="collapseDashboards">
                            <div class="sb-nav-link-icon"></div>
                            Inventory
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseSubDashboards2" >
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('dashboards.inventory') }}">Boiler 1</a>
                                <a class="nav-link" href="{{ route('dashboards.inventoryB2') }}">Boiler 2</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseSubDashboards3" aria-expanded="false" aria-controls="collapseDashboards">
                            <div class="sb-nav-link-icon"></div>
                            Infra
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseSubDashboards3" >
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('dashboards.infra') }}">Boiler 1</a>
                                <a class="nav-link" href="{{ route('dashboards.infraB2') }}">Boiler 2</a>
                            </nav>
                        </div>
                    </nav>
                </div>

                <!--Control setting section -->
                <div class="sb-sidenav-menu-heading">Operation</div>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseControls" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Manual & Auto Controls
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseControls" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{ route('controls.broilerTemperature') }}">Broiler Temperature</a>
                        <a class="nav-link" href="{{ route('controls.ventilationSystem') }}">Ventilation System</a>
                        <a class="nav-link" href="{{ route('controls.lightDimmer') }}">Light Dimmer</a>
                        <a class="nav-link" href="{{ route('controls.autoFeederSystem') }}">Automatic Feeder</a>
                        <a class="nav-link" href="{{ route('controls.waterDosing') }}">Water Dosing</a>
                        <a class="nav-link" href="{{ route('controls.heater') }}">Heaters</a>
                        <a class="nav-link" href="{{ route('controls.staticPressure') }}">Static Pressure</a>
                        <a class="nav-link" href="{{ route('controls.tunnelCurtain') }}">Tunnel Curtains</a>
                    </nav>
                </div>
                <!--Management-->
                <div class="sb-sidenav-menu-heading">Management</div>
                <!--Inventory-->
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts1" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Inventory
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts1" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{ route('inventories.doc') }}">DOC</a>
                        <a class="nav-link" href="{{ route('inventories.feed') }}">Feed</a>
                        <a class="nav-link" href="{{ route('inventories.supplements') }}">Suppliments</a>
                        <a class="nav-link" href="{{ route('inventories.cleaningChemicals') }}">Cleaning chemicals</a>
                    </nav>
                </div>
                <!--Weight-->
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages1" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Weights and FOC
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsePages1" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Cycle Record
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="login.html">Login</a>
                                <a class="nav-link" href="register.html">Register</a>
                                <a class="nav-link" href="password.html">Forgot Password</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                            Sloter house
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="401.html">401 Page</a>
                                <a class="nav-link" href="404.html">404 Page</a>
                                <a class="nav-link" href="500.html">500 Page</a>
                            </nav>
                        </div>
                    </nav>
                </div>
                <!--Addons-->
                <div class="sb-sidenav-menu-heading">Addons</div>
                <a class="nav-link" href="{{ route('addOns.chart') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Charts
                </a>
                <a class="nav-link" href="{{ route('addOns.cycleRecord') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Cycle Records
                </a>
            </div>
        </div>
        <!--Login info-->
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            MyDGtech Developer
        </div>
    </nav>
</div>
