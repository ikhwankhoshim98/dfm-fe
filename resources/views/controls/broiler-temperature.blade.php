<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DFM: Broiler Temperature</title>
        <link rel="stylesheet" href="<?php echo asset('css/styles.css')?>" type="text/css">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

    </head>
    <body class="sb-nav-fixed">
        <!-- header -->
        @include('header')
        <div id="layoutSidenav">
            <!-- sidebar -->
            @include('side-bar')
            <!-- page content -->
            <div id="layoutSidenav_content">
                <main>
                    <h1 class="text-center p-4 mt-4 pt-4 fw-bold">Broiler Temperature</h1>
                    <section class="pt-4">
                        <div class="container px-lg-5">
                            <!-- Page Features-->
                            <div class="justify-content-center">
                                <!-- Silo Scale -->
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"></i> Live Average Temperature</h6>
                                    </div>
                                    <div class="row p-4 pb-0">
                                        <!-- B1 Avg Temperature -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-primary shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">B1 Avg Temperature
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">00.00 °C</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- B1 Temperature SP -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-primary shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">B1 Temperature SP
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">00.00 °C</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- B2 Avg Temperature -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-danger shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">B2 Avg Temperature
                                                            </div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800">00.00 °C</div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- B2 TEMPERATURE SP -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-danger shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">B2 TEMPERATURE SP
                                                            </div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800">00.00 °C</div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-comments fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end live average temp -->
                                <!-- Broiler Temperature Chart -->
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"></i> Broiler Temperature Chart</h6>
                                    </div>
                                    <div class="row p-4 pb-0">
                                        <!--  Broiler 1 Avg. Temprature -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1"> Broiler 1 Avegerage Temprature
                                                    </div>
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="card-body">
                                                            <canvas id="dailyTemperature"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Broiler 2 Avg. Temprature -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Broiler 2 Avegerage Temprature
                                                    </div>
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="card-body">
                                                            <canvas id="cycleTemperature"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- end Broiler Temoerature Chart -->
                                <!-- Zone Temperature Charts -->
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"></i> Zone Temperature Charts</h6>
                                    </div>
                                    <div class="row p-4 pb-0">
                                        <!--  Broiler 1 Avg. Temprature -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1"> Broiler 1 Zone Temperature
                                                    </div>
                                                    <div class="justify-content-center">
                                                        <table class="table">
                                                            <thead>
                                                              <tr>
                                                                <th scope="col">Zone</th>
                                                                <th scope="col">Temperature</th>
                                                                <th scope="col">Humidity</th>
                                                                <th scope="col">NH4 Level</th>
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                              <tr>
                                                                <th scope="row">1</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                              <tr>
                                                                <th scope="row">2</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                              <tr>
                                                                <th scope="row">3</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                              <tr>
                                                                <th scope="row">4</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Broiler 2 Avg. Temprature -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Broiler 2 Zone Temperature
                                                    </div>
                                                    <div class="justify-content-center">
                                                        <table class="table">
                                                            <thead>
                                                              <tr>
                                                                <th scope="col">Zone</th>
                                                                <th scope="col">Temperature</th>
                                                                <th scope="col">Humidity</th>
                                                                <th scope="col">NH4 Level</th>
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                              <tr>
                                                                <th scope="row">1</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                              <tr>
                                                                <th scope="row">2</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                              <tr>
                                                                <th scope="row">3</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                              <tr>
                                                                <th scope="row">4</th>
                                                                <td>00.00 °C</td>
                                                                <td>00.00 °C</td>
                                                                <td>0</td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- end Zone Temperature Charts -->

                            </div>
                    </section>
                </main>
                <!-- footer -->
                @include('footer')
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="include.js"></script>
        <script src="./js/scripts.js"></script>

        <script src="../uibuilder/vendor/socket.io/socket.io.js"></script>
        <script src="./uibuilderfe.min.js"></script>
        <script src="./js/getput.js"></script>
        <script src="assets/demo/charts.js"></script>
        <script src="assets/demo/B1avgtempchart.js"></script>
        <script src="assets/demo/B2avgtempchart.js"></script>
        <script src="assets/demo/B2weighchart.js"></script>
        <script src="assets/demo/B1weighchart.js"></script>

        <!-- charts/grapgh -->
        <script type="text/javascript" src="{{ URL::asset('vendor/chart.js/Chart.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/charts.js') }}"></script>

    </body>
</html>
