<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DFM: Automatic Light Dimmer</title>
        <link rel="stylesheet" href="<?php echo asset('css/styles.css')?>" type="text/css">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

    </head>
    <body class="sb-nav-fixed">
        <!-- header -->
        @include('header')
        <div id="layoutSidenav">
            <!-- sidebar -->
            @include('side-bar')
            <!-- page content -->
            <div id="layoutSidenav_content">
                <main>
                    <h1 class="text-center p-4 mt-4 pt-4 fw-bold">Automatic Light Dimmer</h1>
                    <section class="pt-4">
                        <div class="container px-lg-5">
                            <h2 class="p-4 mt-4 pb-0 pt-4">Light system settings</h2>
                            <!-- boiler one  -->
                            <div class="justify-content-center">
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="glyphicon glyphicon-fire"></i> Broiler One</h6>
                                    </div>
                                    <div class="d-flex justify-content-center p-4 pt-0 pb-0">
                                        <!-- Manual -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Manual Light Control
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Current Value
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">4.0</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Desired Value
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">4.5</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- switch ON/OFF fan -->
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Light Adjustment</div>
                                                                <label for="customRange2" class="form-label">Adjust the light by setting the value using the slider :</label>
                                                                <input type="range" class="form-range" min="0" max="5" step="0.5" id="customRange3">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Auto -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Auto Light Control
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">DAYLIGHT CYCLE METHOD
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Method 2</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Feeding Method
                                                                        </div>
                                                                        <div class="col mr-2">
                                                                            <select class="browser-default custom-select col-12">
                                                                                <option selected>Select Feeding Method</option>
                                                                                <option value="1">Method 1</option>
                                                                                <option value="2">Method 2</option>
                                                                                <option value="3">Method 3</option>
                                                                            </select>
                                                                        </div>
                                                                        <button class="btn btn-primary m-0 mt-2">Set</button>
                                                                        <div class="justify-content-center">
                                                                            <table class="table">
                                                                                <thead>
                                                                                  <tr>
                                                                                    <th scope="col">Method</th>
                                                                                    <th scope="col">Start From</th>
                                                                                    <th scope="col">End At</th>
                                                                                    <th scope="col">Hours</th>
                                                                                    <th scope="col">Cycle</th>
                                                                                  </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                  <tr>
                                                                                    <th scope="row">1</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                  <tr>
                                                                                    <th scope="row">2</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                  <tr>
                                                                                    <th scope="row">3</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                  <tr>
                                                                                    <th scope="row">4</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                </tbody>
                                                                              </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end boiler one -->
                            <!-- boiler two -->
                            <div class="justify-content-center">
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="glyphicon glyphicon-fire"></i> Broiler Two</h6>
                                    </div>
                                    <div class="d-flex justify-content-center p-4 pt-0 pb-0">
                                        <!-- Manual -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Manual Light Control
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Current Value
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">3.5</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Desired Value
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">4.5</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- switch ON/OFF fan -->
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Light Adjustment</div>
                                                                <label for="customRange2" class="form-label">Adjust the light by setting the value using the slider :</label>
                                                                <input type="range" class="form-range" min="0" max="5" step="0.5" id="customRange3">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Auto -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Auto Light Control
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Daylight Cycle Method
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Method 1</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Feeding Method
                                                                        </div>
                                                                        <div class="col mr-2">
                                                                            <select class="browser-default custom-select col-12">
                                                                                <option selected>Select Feeding Method</option>
                                                                                <option value="1">Method 1</option>
                                                                                <option value="2">Method 2</option>
                                                                                <option value="3">Method 3</option>
                                                                            </select>
                                                                        </div>
                                                                        <button class="btn btn-primary m-0 mt-2">Set</button>
                                                                        <div class="justify-content-center">
                                                                            <table class="table">
                                                                                <thead>
                                                                                  <tr>
                                                                                    <th scope="col">Method</th>
                                                                                    <th scope="col">Start From</th>
                                                                                    <th scope="col">End At</th>
                                                                                    <th scope="col">Hours</th>
                                                                                    <th scope="col">Cycle</th>
                                                                                  </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                  <tr>
                                                                                    <th scope="row">1</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                  <tr>
                                                                                    <th scope="row">2</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                  <tr>
                                                                                    <th scope="row">3</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                  <tr>
                                                                                    <th scope="row">4</th>
                                                                                    <td>7/3/22 (Mon)</td>
                                                                                    <td>13/3/22 (Sun)</td>
                                                                                    <td>5</td>
                                                                                    <td>10</td>

                                                                                  </tr>
                                                                                </tbody>
                                                                              </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Manual Light Control Velocity  -->
                                    <!-- <div class="col-xl-12 col-md-12 mb-4 p-4 pt-0 pb-0">
                                        <div class="card shadow h-100 py-2">
                                            <div class="card-body">
                                                <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Light Adjustment
                                                </div>

                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <!-- end boiler two -->
                        </div>
                    </section>
                </main>
                <!-- footer -->
                @include('footer')
            </div>
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="include.js"></script>
        <script src="./js/scripts.js"></script>

        <script src="../uibuilder/vendor/socket.io/socket.io.js"></script>
        <script src="./uibuilderfe.min.js"></script>
        <script src="./js/getput.js"></script>
        <script src="assets/demo/charts.js"></script>
        <script src="assets/demo/B1avgtempchart.js"></script>
        <script src="assets/demo/B2avgtempchart.js"></script>
        <script src="assets/demo/B2weighchart.js"></script>
        <script src="assets/demo/B1weighchart.js"></script>

        <!-- charts/grapgh -->
        <script type="text/javascript" src="{{ URL::asset('vendor/chart.js/Chart.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/charts.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/progressCycle.js') }}"></script>

    </body>
</html>
