<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DFM: Ventilation System</title>
        <link rel="stylesheet" href="<?php echo asset('css/styles.css')?>" type="text/css">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    </head>
    <body class="sb-nav-fixed">
        <!-- header -->
        @include('header')
        <div id="layoutSidenav">
            <!-- sidebar -->
            @include('side-bar')
            <!-- page content -->
            <div id="layoutSidenav_content">
                <main>
                    <h1 class="text-center mt-4 p-4 pt-4 fw-bold">Ventilation System</h1>
                    <section class="pt-4">
                        <div class="container px-lg-5">
                            <!-- boiler one  -->
                            <div class="justify-content-center">
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="glyphicon glyphicon-fire"></i> Broiler One</h6>
                                    </div>
                                    <div class="d-flex justify-content-center p-4 pt-0 pb-0">
                                        <!-- Manual -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Manual Ventilation
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Controller Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- switch ON/OFF fan -->
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">ventilation Velocity Set Point</div>
                                                                <div class="d-flex justify-content-center col-md-6 col-lg-12">
                                                                    <button class="btn btn-primary  btn-sm m-2" style="background-color: #0d6efd; opacity: 1;" disabled>Selected Fan</button>
                                                                    <button class="btn btn-sm m-2" style="background-color: #ccc; opacity: 1;" disabled>Unselected Fan</button>
                                                                </div>
                                                                <div class="d-flex flex-wrap justify-content-center pt-2 pb-0 p-4" id="seatsBlock">
                                                                    <label><input type="checkbox" class="fan" value="A1"><span class="label">A1</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A2"><span class="label">A2</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A3"><span class="label">A3</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A4"><span class="label">A4</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A5"><span class="label">A5</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A6"><span class="label">A6</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A7"><span class="label">A7</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A8"><span class="label">A8</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A9"><span class="label">A9</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A10"><span class="label">A10</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A11"><span class="label">A11</span></label>
                                                                </div>
                                                                <div class="d-flex flex-wrap justify-content-center pt-0 pb-2 p-4" id="seatsBlock">
                                                                    <label><input type="checkbox" class="fan" value="B1"><span class="label">B1</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B2"><span class="label">B2</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B3"><span class="label">B3</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B4"><span class="label">B4</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B5"><span class="label">B5</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B6"><span class="label">B6</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B7"><span class="label">B7</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B8"><span class="label">B8</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B9"><span class="label">B9</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B10"><span class="label">B10</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B11"><span class="label">B11</span></label>
                                                                </div>
                                                                <div class="d-flex justify-content-center">
                                                                    <button class="btn btn-primary m-2" onclick="onFan()">On</button>
                                                                    <button class="btn btn-danger m-2" onclick="offFan()">Off</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Auto -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Auto Ventilation
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Controller Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Average Temperature Chart
                                                                        </div>
                                                                        <div class="row no-gutters align-items-center">
                                                                            <div class="card-body">
                                                                                <canvas id="dailyTemperature"></canvas>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Manual Ventilation Velocity  -->
                                    <!-- <div class="col-xl-12 col-md-12 mb-4 p-4 pt-0 pb-0">
                                        <div class="card shadow h-100 py-2">
                                            <div class="card-body">
                                                <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Ventilation Velocity Set Point
                                                </div>

                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <!-- end boiler one -->
                            <!-- boiler two -->
                            <div class="justify-content-center">
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="glyphicon glyphicon-fire"></i> Broiler Two</h6>
                                    </div>
                                    <div class="d-flex justify-content-center p-4 pt-0 pb-0">
                                        <!-- Manual -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Manual Ventilation
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Controller Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- switch ON/OFF fan -->
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">ventilation Velocity Set Point</div>
                                                                <div class="d-flex justify-content-center col-md-6 col-lg-12">
                                                                    <button class="btn btn-primary  btn-sm m-2" style="background-color: #0d6efd; opacity: 1;" disabled>Selected Fan</button>
                                                                    <button class="btn btn-sm m-2" style="background-color: #ccc; opacity: 1;" disabled>Unselected Fan</button>
                                                                </div>
                                                                <div class="d-flex flex-wrap justify-content-center pt-2 pb-0 p-4" id="seatsBlock">
                                                                    <label><input type="checkbox" class="fan" value="A1"><span class="label">A1</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A2"><span class="label">A2</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A3"><span class="label">A3</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A4"><span class="label">A4</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A5"><span class="label">A5</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A6"><span class="label">A6</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A7"><span class="label">A7</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A8"><span class="label">A8</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A9"><span class="label">A9</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A10"><span class="label">A10</span></label>
                                                                    <label><input type="checkbox" class="fan" value="A11"><span class="label">A11</span></label>
                                                                </div>
                                                                <div class="d-flex flex-wrap justify-content-center pt-0 pb-2 p-4" id="seatsBlock">
                                                                    <label><input type="checkbox" class="fan" value="B1"><span class="label">B1</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B2"><span class="label">B2</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B3"><span class="label">B3</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B4"><span class="label">B4</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B5"><span class="label">B5</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B6"><span class="label">B6</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B7"><span class="label">B7</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B8"><span class="label">B8</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B9"><span class="label">B9</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B10"><span class="label">B10</span></label>
                                                                    <label><input type="checkbox" class="fan" value="B11"><span class="label">B11</span></label>
                                                                </div>
                                                                <div class="d-flex justify-content-center">
                                                                    <button class="btn btn-primary m-2" onclick="onFan()">On</button>
                                                                    <button class="btn btn-danger m-2" onclick="offFan()">Off</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Auto -->
                                        <div class="col-xl-6 col-md-6 mb-4 p-2">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="form-switch">
                                                        <input class="form-check-input " type="radio" name="switch" id="B1AutSw">
                                                    </div>
                                                    <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Auto Ventilation
                                                    </div>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-md-6 p-1 pb-0 pt-0 mb-4">
                                                            <div class="card shadow h-100 py-2">
                                                                <div class="card-body">
                                                                    <div class="row no-gutters align-items-center">
                                                                        <div class="col mr-2">
                                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Controller Set Point
                                                                            </div>
                                                                            <div class="row no-gutters align-items-center">
                                                                                <div class="col-auto">
                                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                                </div>
                                                                                <div class="col">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card shadow h-100 py-2">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Average Temperature Chart
                                                                        </div>
                                                                        <div class="row no-gutters align-items-center">
                                                                            <div class="card-body">
                                                                                <canvas id="cycleTemperature"></canvas>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Manual Ventilation Velocity  -->
                                    <!-- <div class="col-xl-12 col-md-12 mb-4 p-4 pt-0 pb-0">
                                        <div class="card shadow h-100 py-2">
                                            <div class="card-body">
                                                <div class="text-xs font-weight-bold text-uppercase p-1 mb-4">Ventilation Velocity Set Point
                                                </div>

                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <!-- end boiler two -->
                        </div>
                    </section>
                </main>
                <!-- footer -->
                @include('footer')
            </div>
        </div>

        <!-- css for fan -->
        <style>
            input[type=checkbox] {
            display: none;
            }
            .label {
            /* border: 1px solid #000; */
            background-color: #ccc;
            display: inline-block;
            padding: 3px;
            margin: 3px;
            /* width: 40px; */
            /* height: 50px; */
            }
            input[type=checkbox]:checked + .label {
            background: #0b5ed7;
            color: #fff;
            }

            .smallBox::before
            {
            content:".";
            width:15px;
            height:15px;
            float:left;
            /* margin-right:10px; */
            }


            input[type=checkbox]:checked:before {
                background-color:Green;
                font-size: 15px;
            }

            .selectedFan::before
            {
            content:"";
            background:#0b5ed7ed;
            }

            .unselectedFan::before
            {
            content:"";
                background-color:#ccc;
            }

            input[type=checkbox] {
                width:25px;
                margin-right:18px;
            }

            input[type=checkbox]:before {
                content: "";
                width: 25px;
                height: 25px;
                display: inline-block;
                vertical-align:middle;
                text-align: center;
                background-color:#ccc;
            }

            input[type=checkbox]:checked:before {
                background-color:#0000;
                /* font-size: 15px; */
            }

        </style>

        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="include.js"></script>
        <script src="./js/scripts.js"></script>

        <script src="../uibuilder/vendor/socket.io/socket.io.js"></script>
        <script src="./uibuilderfe.min.js"></script>
        <script src="./js/getput.js"></script>
        <script src="assets/demo/charts.js"></script>
        <script src="assets/demo/B1avgtempchart.js"></script>
        <script src="assets/demo/B2avgtempchart.js"></script>
        <script src="assets/demo/B2weighchart.js"></script>
        <script src="assets/demo/B1weighchart.js"></script>

        <!-- charts/grapgh -->
        <script type="text/javascript" src="{{ URL::asset('vendor/chart.js/Chart.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/charts.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/fan.js') }}"></script>
    </body>
</html>
