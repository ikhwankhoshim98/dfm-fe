<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DFM: Environment Monitoring</title>
        <link rel="stylesheet" href="<?php echo asset('css/styles.css')?>" type="text/css">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
        <!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css"> -->
    </head>
    <body class="sb-nav-fixed">
        <!-- header -->
        @include('header')
        <div id="layoutSidenav">
            <!-- sidebar -->
            @include('side-bar')
            <div id="layoutSidenav_content">
                <!-- page content -->
                <main>
                    <h1 class="text-center p-4 mt-4 pt-4 fw-bold">Environment Monitoring</h1>
                    <section class="pt-4">
                        <div class="container px-lg-5">
                            <h2 class="p-4 mt-4 pb-0 pt-4">Broiler Two</h2>
                            <!-- Page Features-->
                            <!-- Graph Section -->
                            <div class="justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <!-- daily curve -->
                                    <div class="card shadow m-4" style="width:50%">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary">Daily Curve</h6>
                                        </div>
                                        <div class="card-body">
                                            <canvas id="dailyTemperature"></canvas>
                                        </div>
                                    </div>
                                    <!-- cycle -->
                                    <div class="card shadow m-4" style="width:50%">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary">Cycle Curve</h6>
                                        </div>
                                        <div class="card-body">
                                            <canvas id="cycleTemperature"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <!-- Temperature Section -->
                                <div class="card shadow m-2 p-0 justify-content-center col-sm-2 col-lg-3" >
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-thermometer-empty"></i> Temperature</h6>
                                    </div>
                                    <!-- temperatures -->
                                    <div class="row justify-content-start m-4" >
                                        <!-- monitoring level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Live Zone
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- set point level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Set Point
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Humidity Section -->
                                <div class="card shadow m-2 p-0 justify-content-center col-sm-2 col-lg-3">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-water"></i> Humidity</h6>
                                    </div>
                                    <div class="row justify-content-start m-4">
                                        <!-- monitoring level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Live Zone
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- set point level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Set Point
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Static Pressure -->
                                <div class="card shadow m-2 p-0 justify-content-center col-sm-2 col-lg-3">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-tachometer-alt"></i> Static Pressure</h6>
                                    </div>
                                    <div class="row justify-content-start m-4">
                                        <!-- monitoring level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Live Zone
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- set point level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Set Point
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- CO2 Level -->
                                <div class="card shadow m-2 p-0 justify-content-center col-sm-2 col-lg-3">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-angle-double-up"></i> CO2 Level</h6>
                                    </div>
                                    <div class="row justify-content-start m-4">
                                        <!-- monitoring level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Live Zone
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- set point level -->
                                        <div class="card shadow mb-2 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info mb-1">Set Point
                                                        </div>
                                                        <div class="row no-gutters align-items-center">
                                                            <div class="col-auto">
                                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="progress progress-sm mr-2">
                                                                    <div class="progress-bar bg-info" role="progressbar"
                                                                        style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                        aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Ammonia Level -->
                                <div class="card shadow m-2 p-0 justify-content-center col-sm-2 col-lg-3">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-angle-double-up"></i> Ammonia Level</h6>
                                    </div>
                                    <div class="d-flex justify-content-start">
                                        <!-- temperatures -->
                                        <div class="row justify-content-start m-4" >
                                            <!-- monitoring level -->
                                            <div class="card shadow mb-2 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-info mb-1">Live Zone
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                                </div>
                                                                <div class="col">
                                                                    <div class="progress progress-sm mr-2">
                                                                        <div class="progress-bar bg-info" role="progressbar"
                                                                            style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                            aria-valuemax="100"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- set point level -->
                                            <div class="card shadow mb-2 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-info mb-1">Set Point
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">20°C</div>
                                                                </div>
                                                                <div class="col">
                                                                    <div class="progress progress-sm mr-2">
                                                                        <div class="progress-bar bg-info" role="progressbar"
                                                                            style="width: 70%" aria-valuenow="0" aria-valuemin="0"
                                                                            aria-valuemax="100"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
                <!-- footer -->
                @include('footer')
            </div>
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="include.js"></script>
        <script src="./js/scripts.js"></script>

        <script src="../uibuilder/vendor/socket.io/socket.io.js"></script>
        <script src="./uibuilderfe.min.js"></script>
        <script src="./js/getput.js"></script>
        <script src="assets/demo/charts.js"></script>
        <script src="assets/demo/B1avgtempchart.js"></script>
        <script src="assets/demo/B2avgtempchart.js"></script>
        <script src="assets/demo/B2weighchart.js"></script>
        <script src="assets/demo/B1weighchart.js"></script>

        <!-- charts/grapgh -->
        <script type="text/javascript" src="{{ URL::asset('vendor/chart.js/Chart.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/charts.js') }}"></script>

    </body>
</html>
