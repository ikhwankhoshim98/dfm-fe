<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DFM: Index/Dashboard Copy</title>
        <link rel="stylesheet" href="<?php echo asset('css/styles.css')?>" type="text/css">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

    </head>
    <body class="sb-nav-fixed">
        <!-- header -->
        @include('header')
        <div id="layoutSidenav">
            <!-- sidebar -->
            @include('side-bar')
            <!-- page content -->
            <div id="layoutSidenav_content">
                <main>
                    <h1 class="text-center p-4 mt-4 pt-4 fw-bold">Dashboard</h1>
                    <section class="pt-4">
                        <div class="d-flex justify-content-center container px-lg-5">
                            <!-- boiler one  -->
                            <div class="justify-content-center">
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="glyphicon glyphicon-fire"></i> Broiler One</h6>
                                    </div>
                                    <div class="d-flex justify-content-center p-4 pt-0 pb-0">
                                        <div class="col-xl-12 col-md-12 mb-4">
                                            <div class="card-body">
                                                <div class="d-flex justify-content-center mt-4">
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-success text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">Overall Temperature: 29.3°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Set Point: 28.5°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">OutT: 00.00%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer d-flex align-items-center justify-content-between">
                                                                <a class="small text-white" href="broiler-temperature.html"><div>View Details</div></a>
                                                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-danger text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">Humidity: 25.3°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Set Point: 27.5°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">OutT: 00.00%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-warning text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">NH4 Level: 7
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">CO2 Level: 7
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-primary text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">Livability(%)
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Male: 92%
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Female: 95%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer d-flex align-items-center justify-content-between">
                                                                <a class="small text-white " href="liveability.html"><div>View Details</div></a>
                                                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- average Temperature Chart -->
                                                <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                    <div class="card shadow h-100 py-2">
                                                        <div class="card-body m-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"><span class="fas fa-temperature-high"></span> Average Temperature Chart</div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="card-body">
                                                                    <canvas id="indexChartB1"></canvas>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- average Temperature Chart -->
                                                <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                    <div class="card shadow h-100 py-2">
                                                        <div class="card-body m-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"><span class="fas fa-kiwi-bird"></span> Average Bird Weight</div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="card-body">
                                                                    <canvas id="birdChartB1"></canvas>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end boiler one -->
                            <!-- boiler two -->
                            <div class="justify-content-center">
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="glyphicon glyphicon-fire"></i> Broiler Two</h6>
                                    </div>
                                    <div class="d-flex justify-content-center p-4 pt-0 pb-0">
                                        <div class="col-xl-12 col-md-12 mb-4">
                                            <div class="card-body">
                                                <div class="d-flex justify-content-center mt-4">
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-success text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">Overall Temperature: 29.3°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Set Point: 28.5°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">OutT: 00.00%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer d-flex align-items-center justify-content-between">
                                                                <a class="small text-white" href="broiler-temperature.html"><div>View Details</div></a>
                                                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-danger text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">Humidity: 25.3°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Set Point: 27.5°C
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">OutT: 00.00%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-warning text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">NH4 Level: 7
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">CO2 Level: 7
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-3 p-1 pb-0 pt-0 mb-4">
                                                        <div class="card bg-primary text-white shadow h-100 py-2 pb-0">
                                                            <div class="card-body">
                                                                <div class="row no-gutters align-items-center">
                                                                    <div class="col mr-2">
                                                                        <div class="h5 text-white mb-3">Livability(%)
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Male: 92%
                                                                        </div>
                                                                        <div class="h5 text-white mb-3">Female: 95%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer d-flex align-items-center justify-content-between">
                                                                <a class="small text-white " href="liveability.html"><div>View Details</div></a>
                                                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- average Temperature Chart -->
                                                <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                    <div class="card shadow h-100 py-2">
                                                        <div class="card-body m-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"><span class="fas fa-temperature-high"></span> Average Temperature Chart</div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="card-body">
                                                                    <canvas id="indexChartB2"></canvas>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- average Temperature Chart -->
                                                <div class="col-xl-12 col-md-12 p-1 pb-0 pt-0 mb-4">
                                                    <div class="card shadow h-100 py-2">
                                                        <div class="card-body m-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"><span class="fas fa-kiwi-bird"></span> Average Bird Weight</div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="card-body">
                                                                    <canvas id="birdChartB2"></canvas>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end boiler two -->
                        </div>
                    </section>
                </main>
                <!-- footer -->
                @include('footer')
            </div>
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="include.js"></script>
        <script src="./js/scripts.js"></script>

        <script src="../uibuilder/vendor/socket.io/socket.io.js"></script>
        <script src="./uibuilderfe.min.js"></script>
        <script src="./js/getput.js"></script>
        <script src="assets/demo/charts.js"></script>
        <script src="assets/demo/B1avgtempchart.js"></script>
        <script src="assets/demo/B2avgtempchart.js"></script>
        <script src="assets/demo/B2weighchart.js"></script>
        <script src="assets/demo/B1weighchart.js"></script>

        <!-- charts/grapgh -->
        <script src="vendor/chart.js/Chart.min.js"></script>
        <script src="js/indexCharts.js"></script>

        <script>
            $(function() {

            $(".progressCycle").each(function() {

              var value = $(this).attr('data-value');
              var left = $(this).find('.progress-left .progress-bar');
              var right = $(this).find('.progress-right .progress-bar');

              if (value > 0) {
                if (value <= 50) {
                  right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
                } else {
                  right.css('transform', 'rotate(180deg)')
                  left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
                }
              }

            })

            function percentageToDegrees(percentage) {

              return percentage / 100 * 360

            }

            });
        </script>
    </body>
</html>
