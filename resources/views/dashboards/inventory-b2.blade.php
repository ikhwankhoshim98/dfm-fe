<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DFM: Inventory</title>
        <link rel="stylesheet" href="<?php echo asset('css/styles.css')?>" type="text/css">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

    </head>
    <body class="sb-nav-fixed">
        <!-- header -->
        @include('header')
        <div id="layoutSidenav">
            <!-- sidebar -->
            @include('side-bar')
            <!-- page content -->
            <div id="layoutSidenav_content">
                <main>
                    <h1 class="text-center p-4 mt-4 pt-4 fw-bold">Inventory</h1>
                    <section class="pt-4">
                        <div class="container px-lg-5">
                            <h2 class="p-4 mt-4 pb-0 pt-4">Broiler Two</h2>
                            <!-- Page Features-->
                            <div class="justify-content-center">
                                <!-- Silo Scale -->
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="fas fa-balance-scale"></i> Silo Scale</h6>
                                    </div>
                                    <div class="row p-4 pb-0">
                                        <!-- Current Level -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-danger shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Current Level
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                    <div class="progress progress-sm mr-2">
                                                                        <div class="progress-bar bg-danger" role="progressbar"
                                                                            style="width:30%" aria-valuenow="50" aria-valuemin="0"
                                                                            aria-valuemax="100"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Last Discharged -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-success shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Last Discharged
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Last Refill -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-primary shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">
                                                                Last Refill</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800">unknown</div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Cycle Consumption -->
                                        <div class="col-xl-3 col-md-6 mb-4">
                                            <div class="card border-left-warning shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                                Cycle Consumption</div>
                                                            <div class="h5 mb-0 font-weight-bold text-gray-800">unknown</div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-comments fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end silo scale -->
                                <!-- Broiler Water -->
                                <div class="card shadow m-4">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="fa fa-fire"></i> Broiler Water</h6>
                                    </div>
                                    <div class="row p-4 pb-0">
                                        <!-- tank level -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Tank Level
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <!-- ph level -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">PH Level
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- cycle flow -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="text-xs font-weight-bold text-uppercase mb-1">Cycle Flow
                                                    </div>
                                                    <div class="progressCycle mx-auto" data-value='80'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar border-primary"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar border-primary"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                        <div class="h2 font-weight-bold">80%</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- cycle consumption -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="text-xs font-weight-bold text-uppercase mb-1">Cycle Consumption
                                                    </div>
                                                    <div class="progressCycle mx-auto" data-value='60'>
                                                        <span class="progress-left">
                                                            <span class="progress-bar border-primary"></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar border-primary"></span>
                                                        </span>
                                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                                        <div class="h2 font-weight-bold">60%</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end broiler water -->
                                <!-- Supplement -->
                                <div class="card shadow m-4 row justify-content-center">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="fas fa-air-freshener"></i> Supplement</h6>
                                    </div>
                                    <div class="row p-4 pb-0">
                                        <!-- amount -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Amount
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- occurance -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Occurance
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end supplement -->
                                <!-- Medication -->
                                <div class="card shadow m-4 row justify-content-center">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="fas fa-briefcase-medical"></i> Medication</h6>
                                    </div>
                                    <div class="row p-4 pb-0">
                                        <!-- amount -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Amount
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- occurance -->
                                        <div class="col-xl-6 col-md-6 mb-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Occurance
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end medication -->
                                <!-- DOC Cycle -->
                                <div class="card shadow m-4 row justify-content-center">
                                    <div class="card-main-header py-3">
                                        <h6 class="m-0 font-weight-bold"><i class="fas fa-circle-notch"></i> DOC Cycle</h6>
                                    </div>
                                    <div class="row justify-content-center p-4">
                                        <!-- arrival count -->
                                        <div class="col-xl-4 col-md-4 p-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Arrival Amount
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- mortality -->
                                        <div class="col-xl-4 col-md-4 p-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Mortality
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- available harvest -->
                                        <div class="col-xl-4 col-md-4 p-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Available for Harvest
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- amount harvest -->
                                        <div class="col-xl-4 col-md-4 p-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Amount Harvested
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- cumulative harvested weight -->
                                        <div class="col-xl-4 col-md-4 p-4">
                                            <div class="card shadow h-100 py-2">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Cumulative Harvested Weight
                                                            </div>
                                                            <div class="row no-gutters align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">unknown</div>
                                                                </div>
                                                                <div class="col">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <!-- <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end DOC cycle -->
                            </div>
                    </section>
                </main>
                <!-- footer -->
                @include('footer')
            </div>
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="include.js"></script>
        <script src="./js/scripts.js"></script>

        <script src="../uibuilder/vendor/socket.io/socket.io.js"></script>
        <script src="./uibuilderfe.min.js"></script>
        <script src="./js/getput.js"></script>
        <script src="assets/demo/charts.js"></script>
        <script src="assets/demo/B1avgtempchart.js"></script>
        <script src="assets/demo/B2avgtempchart.js"></script>
        <script src="assets/demo/B2weighchart.js"></script>
        <script src="assets/demo/B1weighchart.js"></script>

        {{-- charts/grapgh --}}
        <script type="text/javascript" src="{{ URL::asset('vendor/chart.js/Chart.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/charts.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/cycle-bar.js') }}"></script>

    </body>
</html>
