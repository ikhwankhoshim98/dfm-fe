<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DFM: Infa Broile Two</title>
        <link rel="stylesheet" href="<?php echo asset('css/styles.css')?>" type="text/css">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

    </head>
    <body class="sb-nav-fixed">
        <!-- header -->
        @include('header')
        <div id="layoutSidenav">
            <!-- sidebar -->
            @include('side-bar')
            <!-- page content -->
            <div id="layoutSidenav_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="text-center mt-5">
                                    <h1 class="display-1">Infra(Broiler Two)</h1>
                                    <p class="lead">Under constraction</p>
                                    <p>This page is under construction.</p>
                                    <a href="{{ route('dashboards.index') }}">
                                        <i class="fas fa-arrow-left me-1"></i>
                                        Return to Dashboard
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <!-- footer -->
                @include('footer')
            </div>
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="include.js"></script>
        <script src="./js/scripts.js"></script>

        <script src="../uibuilder/vendor/socket.io/socket.io.js"></script>
        <script src="./uibuilderfe.min.js"></script>
        <script src="./js/getput.js"></script>
        <script src="assets/demo/charts.js"></script>
        <script src="assets/demo/B1avgtempchart.js"></script>
        <script src="assets/demo/B2avgtempchart.js"></script>
        <script src="assets/demo/B2weighchart.js"></script>
        <script src="assets/demo/B1weighchart.js"></script>

        <!-- charts/grapgh -->
        <!-- <script src="js/cycle-bar.js"></script> -->

        <script>
            $(function() {

            $(".progressCycle").each(function() {

              var value = $(this).attr('data-value');
              var left = $(this).find('.progress-left .progress-bar');
              var right = $(this).find('.progress-right .progress-bar');

              if (value > 0) {
                if (value <= 50) {
                  right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
                } else {
                  right.css('transform', 'rotate(180deg)')
                  left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
                }
              }

            })

            function percentageToDegrees(percentage) {

              return percentage / 100 * 360

            }

            });
        </script>
    </body>
</html>
